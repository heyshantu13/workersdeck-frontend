import { makeStyles } from '@mui/styles';

const customStyle = makeStyles({

      wd_select:{
        background: '#FFFFFF',
        boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
        borderRadius: '8px',
      },
      wd_go_btn:{
        width: '120px',
        height: '52px',
        background: '#2D3748',
        boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
        borderRadius: '8px'
      },
      wd_service_card:{
          marginTop: '-2rem',
          boxShadow: '0px 3px 6px rgba(0, 0, 0, 0.1), 0px 4px 8px rgba(0, 0, 0, 0.08), 0px 1px 12px rgba(0, 0, 0, 0.04)',
          borderRadius: '12px',
          maxWidth:'96rem',
      },
      wd_offer_card:{
        background: '#3F51B5',
        boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
        borderRadius: '2px',
        height: '415px',
      },
      offetTextOne: {
        fontFamily: "Poppins",
        fontStyle: "normal",
        fontWeight: "bold",
        fontSize: "38px",
        lineHeight: "45px",
        marginTop: "67px",
        textAlign: "center",
        letterSpacing: "0.08em",
        color: "#ffffff",
      },
      offetTextTwo: {
        fontFamily: "Poppins",
        fontStyle: "normal",
        fontWeight: "bold",
        fontSize: "38px",
        lineHeight: "45px",
        marginTop: "24px",
        textAlign: "center",
        letterSpacing: "0.08em",
        color: "#ffffff",
      },
      bookNowBtn: {
        background: "#2d3748 !important",
        boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25) !important",
        borderRadius: "8px !important",
        fontStyle: "normal",
        fontWeight: "bold !important",
        fontSize: "24px !important",
        lineHeight: "36px !important",
        width: "355px",
        height: "52px",
        color: "#ffffff !important",
        textAlign: "center",
        marginTop: "66px",
      },

});

export default customStyle;